package main

import (
	"os"
	"fmt"
	"log"
	"bufio"
	"strconv"
	"strings"
	"unicode"
)

type ExprKind uint64
type TokenKind uint64

const (
	Num TokenKind = iota
	Add
	Sub
	Mul
	Div
	LPa
	RPa
	Eol
)

const (
	ExprNum ExprKind = iota
	ExprAdd
	ExprSub
	ExprMul
	ExprDiv
)

type Token struct {
	value string
	kind  TokenKind
}

type Expr struct {
	value string
	kind  ExprKind
	left  *Expr
	right *Expr
	has_precedence bool
}

func chopString(str *string, count int) string {
	var ret string

	for i := 0; i < count; i++ {
		ret += string((*str)[0])
		*str = strings.TrimPrefix(*str, string((*str)[0]))
	}

	return ret
}

func lexExpr(expr_str string) []Token {
	var tokens []Token

	for expr_str[0] != ';' {
		var token Token

		if expr_str[0] == ' ' || expr_str[0] == '\t' || expr_str[0] == '\n' {
			chopString(&expr_str, 1)
			continue
		}

		if unicode.IsDigit(rune(expr_str[0])) {
			token.kind = Num

			for unicode.IsDigit(rune(expr_str[0])) { token.value += chopString(&expr_str, 1) }

			tokens = append(tokens, token)
		} else {
			switch expr_str[0] {
			case '+':
				token.kind = Add
				token.value = chopString(&expr_str, 1)
				tokens = append(tokens, token)
			case '-':
				token.kind = Sub
				token.value = chopString(&expr_str, 1)
				tokens = append(tokens, token)
			case '*':
				token.kind = Mul
				token.value = chopString(&expr_str, 1)
				tokens = append(tokens, token)
			case '/':
				token.kind = Div
				token.value = chopString(&expr_str, 1)
				tokens = append(tokens, token)
			case '(':
				token.kind = LPa
				token.value = chopString(&expr_str, 1)
				tokens = append(tokens, token)
			case ')':
				token.kind = RPa
				token.value = chopString(&expr_str, 1)
				tokens = append(tokens, token)
			default:
				log.Fatalf("Encountered unknown token: %c\n", expr_str[0])
			}
		}
	}

	tokens = append(tokens, Token{
		value: ";",
		kind: Eol,
	})

	return tokens
}

func newNumExpr(value string) *Expr {
	return &Expr{
		value: value,
		kind: ExprNum,
		left: nil,
		right: nil,
		has_precedence: true,
	}
}

func parseTerm(expr_toks []Token, index *int) *Expr {
	var expr *Expr

	switch expr_toks[*index].kind {
	case Num:
		expr = newNumExpr(expr_toks[*index].value)

		(*index)++
	case LPa:
		(*index)++

		expr = parseExpr(expr_toks, index, true)

		if expr_toks[(*index)].kind != RPa {
			log.Fatalf("Was expecting `)`, but got: `%+v`\n", expr_toks[*index])
		}

		(*index)++
	default:
		log.Fatalf("Unexpected token: %+v\n", expr_toks[*index])
	}

	return expr
}

func parseExpr(expr_toks []Token, index *int, precedence bool) *Expr {
	var a *Expr

	a = parseTerm(expr_toks, index)

	for true {
		var tmp Expr

		switch expr_toks[*index].kind {
		case Add:
			(*index)++

			tmp = Expr{
				kind: ExprAdd,
				value: "(" + expr_toks[*(index) - 2].value + ")",
				left: a,
				right: parseTerm(expr_toks, index),
				has_precedence: precedence,
			}

			a = &tmp
		case Sub:
			(*index)++

			tmp = Expr{
				kind: ExprSub,
				value: "(" + expr_toks[*(index) - 2].value + ")",
				left: a,
				right: parseTerm(expr_toks, index),
				has_precedence: precedence,
			}

			a = &tmp
		case Mul:
			(*index)++

			tmp = Expr{
				kind: ExprMul,
				value: "(" + expr_toks[*(index) - 1].value + ")",
				left: nil,
				right: nil,
				has_precedence: precedence,
			}

			tmp.right = parseTerm(expr_toks, index)

			if ((*a).has_precedence) {
				tmp.left = a
				a = &tmp
			} else {
				tmp.left = a.right
				a.right = &tmp
			}
		case Div:
			(*index)++

			tmp = Expr{
				kind: ExprDiv,
				value: "(" + expr_toks[*(index) - 1].value + ")",
				left: nil,
				right: nil,
				has_precedence: precedence,
			}

			tmp.right = parseTerm(expr_toks, index)

			if ((*a).has_precedence) {
				tmp.left = a
				a = &tmp
			} else {
				tmp.left = a.right
				a.right = &tmp
			}
		default:
			return a
		}
	}

	return nil
}

func simplifyExpr(expr *Expr) *Expr {
	if (*expr).kind == ExprNum {
		return expr
	} else {
		a, err_a := strconv.Atoi(simplifyExpr((*expr).left).value)
		b, err_b := strconv.Atoi(simplifyExpr((*expr).right).value)

		if err_a == nil && err_b == nil {
			switch (*expr).kind {
			case ExprAdd: return newNumExpr(strconv.Itoa(a + b))
			case ExprSub: return newNumExpr(strconv.Itoa(a - b))
			case ExprMul: return newNumExpr(strconv.Itoa(a * b))
			case ExprDiv: return newNumExpr(strconv.Itoa(a / b))
			default:
				log.Fatalf("Unreachable.\n")
				return nil
			}
		} else { return nil }
	}
}

func printDepth(depth int) {
	if depth == 0 {
		fmt.Printf("|- ")
		return
	}

	for i := 0; i < depth; i++ {
		fmt.Printf("|  ")
	}

	fmt.Printf("`- ")
}

func dumpExpr(expr *Expr, depth int) {
	if expr == nil {
		return
	}
	
	printDepth(depth)

	fmt.Printf("%s\n", (*expr).value)
	
	dumpExpr((*expr).left, depth + 1)
	dumpExpr((*expr).right, depth + 1)
}

func main() {
	var expr_str string
	var expr_toks_index int = 0
	scanner := bufio.NewScanner(os.Stdin)

	if scanner.Scan() {
		expr_str = scanner.Text()
	}

	expr_str += ";"

	expr_toks := lexExpr(expr_str)
	expr := parseExpr(expr_toks, &expr_toks_index, false)

	dumpExpr(expr, 0)
	fmt.Printf("-- RESULT --\n")
	dumpExpr(simplifyExpr(expr), 0)
}
