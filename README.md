# GAlg

### _An algebra framework written in Go_

## About

GAlg is to be viewed as a soon-to-be [REPL](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop) to work out algebra expressions and/or equations.
For now this project is WIP and anything can change at any time and it is nowhere as close to usable.
